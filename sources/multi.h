#pragma once
#include <memory>
#include <array>
#include <cstdint>
class MidiMap;

class Multi {
public:
    Multi();
    ~Multi();

    void init(float sample_rate, unsigned buffer_size);
    void load(const MidiMap& midimap);
    void unload();

    void send_midi(uint32_t frame, const uint8_t *message, uint32_t size);
    void render(std::array<float *, 2> outputs, size_t num_frames);

private:
    struct Impl;
    std::unique_ptr<Impl> impl_;
};
