#pragma once
#include <ghc/fs_std.hpp>
#include <vector>

struct MidiPatch {
    unsigned program_number {};
    fs::path sfz_file;
};

struct MidiMap {
    std::vector<MidiPatch> items;
};

MidiMap midi_map_from_yaml_file(const fs::path &path);
