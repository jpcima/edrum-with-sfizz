#include "midimap.h"
#include <yaml-cpp/yaml.h>

MidiMap midi_map_from_yaml_file(const fs::path &path)
{
    MidiMap midimap;
    const fs::path dir = path.parent_path();
    fs::ifstream stream(path);
    YAML::Node doc = YAML::Load(stream);

    for (YAML::const_iterator it = doc.begin(); it != doc.end(); ++it) {
        unsigned program_number = it->first.as<unsigned>() - 1;
        fs::path sfz_file = fs::u8path(it->second.as<std::string>());
        if (sfz_file.is_relative())
            sfz_file = dir / sfz_file;

        MidiPatch patch;
        patch.program_number = program_number;
        patch.sfz_file = std::move(sfz_file);
        midimap.items.push_back(std::move(patch));
    }

    return midimap;
}
