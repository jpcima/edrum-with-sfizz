//          Copyright Jean Pierre Cimalando 2020.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE.md or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#pragma once
#include <fmt/format.h>

class Log {
public:
    template <class... Args>
    static void i(const char *format, Args &&... args)
    {
        generic('-', "info", "\033[36m", format, std::forward<Args>(args)...);
    }

    template <class... Args>
    static void w(const char *format, Args &&... args)
    {
        generic('!', "warn", "\033[33m", format, std::forward<Args>(args)...);
    }

    template <class... Args>
    static void e(const char *format, Args &&... args)
    {
        generic('x', "error", "\033[31m", format, std::forward<Args>(args)...);
    }

    template <class... Args>
    static void s(const char *format, Args &&... args)
    {
        generic('*', "success", "\033[32m", format, std::forward<Args>(args)...);
    }

private:
    template <class... Args>
    static void generic(char symbol, const char *tag, const char *color, const char *format, Args &&... args)
    {
        line(symbol, tag, color, fmt::format(format, std::forward<Args>(args)...).c_str());
    }

    static void line(char symbol, const char *tag, const char *color, const char *text);
};
