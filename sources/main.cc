#include "multi.h"
#include "midimap.h"
#include "logs.h"
#include <jack/jack.h>
#include <jack/midiport.h>
#include <fmt/format.h>
#include <ghc/fs_std.hpp>
#include <unistd.h>
#include <string>
#include <array>
#include <cstdio>

struct AudioContext {
    float sample_rate {};
    unsigned buffer_size {};
    Multi multi;
    jack_port_t *in {};
    jack_port_t *outs[2] {};
};

static int process(jack_nframes_t nframes, void *userdata)
{
    AudioContext &ctx = *static_cast<AudioContext *>(userdata);
    Multi &multi = ctx.multi;

    void *in_buffer = jack_port_get_buffer(ctx.in, nframes);

    for (uint32_t i = 0, n = jack_midi_get_event_count(in_buffer); i < n; ++i) {
        jack_midi_event_t event;
        if (jack_midi_event_get(&event, in_buffer, i) != 0)
            continue;
        multi.send_midi(event.time, event.buffer, event.size);
    }

    std::array<float *, 2> outs {{
        (float *)jack_port_get_buffer(ctx.outs[0], nframes),
        (float *)jack_port_get_buffer(ctx.outs[1], nframes),
    }};

    multi.render(outs, nframes);

    return 0;
}

///
struct jack_client_deleter {
    void operator()(jack_client_t *x)
    {
        jack_client_close(x);
    }
};

typedef std::unique_ptr<jack_client_t, jack_client_deleter> jack_client_u;

///
int main(int argc, char *argv[])
{
    if (argc != 2) {
        fprintf(stderr, "Error: Please indicate the MIDI map file.\n");
        fprintf(stderr, "Usage: edrum <midi-map.yml>\n");
        return 1;
    }
    fs::path midimap_path = fs::u8path(argv[1]);

    Log::i("Start");

    const char *client_name = "edrum";
    Log::i("Opening jack client: {:s}", client_name);

    jack_client_u client{jack_client_open(client_name, JackNoStartServer, nullptr)};
    if (!client) {
        Log::e("Cannot create jack client.");
        return 1;
    }

    AudioContext ctx;

    ctx.sample_rate = jack_get_sample_rate(client.get());
    ctx.buffer_size = jack_get_buffer_size(client.get());

    Log::i("Set up edrum");
    ctx.multi.init(ctx.sample_rate, ctx.buffer_size);

    Log::i("Loading midi map: {:s}", midimap_path.native());
    MidiMap midimap = midi_map_from_yaml_file(midimap_path);
    ctx.multi.load(midimap);

    jack_set_process_callback(client.get(), &process, &ctx);

    ctx.in = jack_port_register(client.get(), "in", JACK_DEFAULT_MIDI_TYPE, JackPortIsInput, 0);

    for (unsigned i = 0; i < 2; ++i) {
        std::string name = fmt::format("out_{:d}", i + 1);
        ctx.outs[i] = jack_port_register(client.get(), name.c_str(), JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
        if (!ctx.outs[i]) {
            Log::e("Cannot register jack output.");
            return 1;
        }
    }

    Log::i("Activating jack client");
    if (jack_activate(client.get()) != 0) {
        Log::e("Could not activate jack client.");
        return 1;
    }

    Log::s("Edrum ready to play");

    for (;;)
        pause();

    return 0;
}
