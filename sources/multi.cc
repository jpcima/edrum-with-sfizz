#include "multi.h"
#include "midimap.h"
#include "lp_1.h"
#include "logs.h"
#include <sfizz.hpp>
#include <nonstd/optional.hpp>
#include <vector>
#include <unordered_map>
#include <mutex>
#include <cstring>

static constexpr float smooth_time_constant = 100e-3f;
static constexpr float fadeout_threshold = 0.0001f;

struct Multi::Impl {
    float sample_rate_ {};
    unsigned buffer_size_ {};

    enum class SlotState {
        Idle,
        Active,
        FadeOut,
    };

    struct Slot {
        std::unique_ptr<sfz::Sfizz> synth_;
        unsigned program_number_ {};
        SlotState state_ = SlotState::Idle;
        lp_1 smooth_gain_;
    };

    std::vector<Slot> slots_;
    std::unordered_map<unsigned, unsigned> slot_of_program_;
    nonstd::optional<unsigned> active_slot_index_;

    std::array<std::unique_ptr<float[]>, 2> temp_buffers_;

    std::mutex render_lock_;

    nonstd::optional<unsigned> get_slot_for_program(unsigned program_number)
    {
        auto it = slot_of_program_.find(program_number);
        if (it == slot_of_program_.end())
            return {};
        return it->second;
    }
};

Multi::Multi()
    : impl_(new Impl)
{
}

Multi::~Multi()
{
}

void Multi::init(float sample_rate, unsigned buffer_size)
{
    Impl &impl = *impl_;

    impl.sample_rate_ = sample_rate;
    impl.buffer_size_ = buffer_size;

    for (std::unique_ptr<float[]> &temp : impl.temp_buffers_)
        temp.reset(new float[buffer_size]);
}

void Multi::load(const MidiMap& midimap)
{
    unload();

    Log::i("Multi: load map");

    Impl &impl = *impl_;
    std::lock_guard<std::mutex> lock(impl.render_lock_);
    std::vector<Impl::Slot> &slots = impl.slots_;

    size_t map_size = midimap.items.size();
    slots.resize(map_size);

    for (size_t i = 0; i < map_size; ++i) {
        Impl::Slot &slot = slots[i];
        const MidiPatch &patch = midimap.items[i];

        Log::i("Multi: `{:s}` in slot {:d} for program {:d}",
               patch.sfz_file.native(), i, patch.program_number);

        sfz::Sfizz *synth = new sfz::Sfizz;
        slot.synth_.reset(synth);
        synth->setSampleRate(impl.sample_rate_);
        synth->setSamplesPerBlock(impl.buffer_size_);
        synth->loadSfzFile(patch.sfz_file);
        slot.program_number_ = patch.program_number;
        impl.slot_of_program_.insert(std::make_pair(patch.program_number, i));

        slot.smooth_gain_.init(impl.sample_rate_);
        slot.smooth_gain_.tau(smooth_time_constant);
    }

    if (!slots.empty()) {
        slots[0].state_ = Impl::SlotState::Active;
        slots[0].smooth_gain_.value(1.0f);
        impl.active_slot_index_ = 0;
    }
}

void Multi::unload()
{
    Log::i("Multi: unload map");

    Impl &impl = *impl_;
    std::lock_guard<std::mutex> lock(impl.render_lock_);
    impl.slots_.clear();
    impl.slot_of_program_.clear();
    impl.active_slot_index_.reset();
}

void Multi::send_midi(uint32_t frame, const uint8_t *message, uint32_t size)
{
    Impl &impl = *impl_;
    std::unique_lock<std::mutex> lock(impl.render_lock_, std::try_to_lock);
    if (!lock.owns_lock())
        return;

    uint8_t status = 0;
    uint8_t data1 = 0;
    uint8_t data2 = 0;

    switch (size) {
    case 3: data2 = message[2] & 127; // fall through
    case 2: data1 = message[1] & 127; // fall through
    case 1: status = message[0]; break;
    default: return;
    }

    if ((status & 0xf0) == 0x90 && data2 == 0)
        status = 0x80 | (status & 0x0f);

    Impl::Slot *active_slot = nullptr;
    nonstd::optional<unsigned> active_slot_index = impl.active_slot_index_;

    if (active_slot_index)
        active_slot = &impl.slots_[*active_slot_index];

    switch (status & 0xf0) {
    case 0x80:
        if (active_slot)
            active_slot->synth_->noteOff(frame, data1, data2);
        break;
    case 0x90:
        if (active_slot)
            active_slot->synth_->noteOn(frame, data1, data2);
        break;
    case 0xb0:
        if (active_slot)
            active_slot->synth_->cc(frame, data1, data2);
        break;
    case 0xc0:
        {
            // program change
            nonstd::optional<unsigned> new_slot_index = impl.get_slot_for_program(data1);
            if (new_slot_index == active_slot_index)
                break;

            if (active_slot_index) {
                Impl::Slot &old_slot = impl.slots_[*active_slot_index];
                old_slot.state_ = Impl::SlotState::FadeOut;
                // TODO: here all-notes-off on old synth, but sfizz doesn't have apparently...
                for (unsigned i = 0; i < 128; ++i)
                    old_slot.synth_->noteOff(frame, i, 0);
            }

            impl.active_slot_index_ = new_slot_index;

            if (new_slot_index) {
                Impl::Slot &new_slot = impl.slots_[*new_slot_index];
                new_slot.state_ = Impl::SlotState::Active;
            }
        }
        break;
    case 0xd0:
        if (active_slot)
            active_slot->synth_->aftertouch(frame, data1);
        break;
    case 0xe0:
        if (active_slot) {
            int bend = ((data1 & 127) | ((data2 & 127) << 7)) - 8192;
            active_slot->synth_->pitchWheel(frame, bend);
        }
        break;
    }
}

void Multi::render(std::array<float *, 2> outputs, size_t num_frames)
{
    Impl &impl = *impl_;

    memset(outputs[0], 0, num_frames * sizeof(float));
    memset(outputs[1], 0, num_frames * sizeof(float));

    std::unique_lock<std::mutex> lock(impl.render_lock_, std::try_to_lock);
    if (!lock.owns_lock())
        return;

    std::array<float *, 2> temp {{
        impl.temp_buffers_[0].get(),
        impl.temp_buffers_[1].get(),
    }};

    std::vector<Impl::Slot> &slots = impl.slots_;

    for (size_t slot_index = 0, slot_count = slots.size(); slot_index < slot_count; ++slot_index) {
        Impl::Slot &slot = slots[slot_index];
        Impl::SlotState state = slot.state_;

        if (state != Impl::SlotState::Idle) {
            slot.synth_->renderBlock(temp.data(), num_frames);
            lp_1 &smooth_gain = slot.smooth_gain_;
            for (size_t i = 0; i < num_frames; ++i) {
                float target_gain = (state != Impl::SlotState::FadeOut) ? 1.0f : 0.0f;
                float current_gain = smooth_gain.compute(target_gain);
                outputs[0][i] += current_gain * temp[0][i];
                outputs[1][i] += current_gain * temp[1][i];
            }
        }

        if (state == Impl::SlotState::FadeOut) {
            lp_1 &smooth_gain = slot.smooth_gain_;
            if (smooth_gain.value() < fadeout_threshold) {
                slot.state_ = Impl::SlotState::Idle;
                smooth_gain.value(0.0f);
            }
        }
    }
}
